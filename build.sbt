name := "receiver"

version := "0.1-SNAPSHOT"

scalaVersion := "2.10.0"

libraryDependencies ++= {
  val akkaVersion       = "2.1.2"
  val camelVersion      = "2.11.0"
  val activeMqVersion   = "5.4.1"
  Seq(
    "org.apache.camel"    %  "camel-core"      % camelVersion,
    "org.apache.camel"    %  "camel-scala"     % camelVersion,
    "org.apache.camel"    %  "camel-jms"       % camelVersion,
    "org.apache.camel"    %  "camel-jackson"   % camelVersion,
    "org.apache.activemq" %  "activemq-core"   % activeMqVersion,
    "org.apache.activemq" %  "activemq-camel"  % activeMqVersion,
    "com.typesafe.akka"   %% "akka-actor"      % akkaVersion,
    "com.typesafe.akka"   %% "akka-slf4j"      % akkaVersion,
    "com.typesafe.akka"   %% "akka-camel"      % akkaVersion,
    "ch.qos.logback"      %  "logback-classic" % "1.0.10"
  )
}