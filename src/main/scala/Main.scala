import akka.camel.{ CamelMessage, Consumer, CamelExtension }
import akka.actor.{ ActorSystem, Props, ActorLogging }
import org.apache.activemq.camel.component.ActiveMQComponent
import org.apache.activemq.camel.component.ActiveMQConfiguration
import org.apache.camel.model.dataformat.JsonLibrary
import java.util.{Map => JMap}
import scala.collection.JavaConversions._

object Main extends App {
    
    val actorSystem = ActorSystem("system")

    val activeMQConfig = new ActiveMQConfiguration()
    activeMQConfig.setBrokerURL("tcp://<activemq hostname>:61616")
    activeMQConfig.setUsePooledConnection(true)
    
    CamelExtension(actorSystem).context.addComponent("activemq", new ActiveMQComponent(activeMQConfig))
    val receiver = actorSystem.actorOf(Props(Receiver))
    
}

object Receiver extends Consumer with ActorLogging {
 
  def endpointUri = "activemq:topic:Assets?selector=contentType='Article' and (eventType in ('Update', 'Create', 'LockOrUnLock'))"
 
  def receive = {
    case msg: CamelMessage => { 
        val bodyMap = msg.bodyAs[JMap[_,_]].toMap
        log.info("{}", bodyMap)
    }
    case _                 => { 
        log.warning("Something else!")
    }
  }
  
  override def onRouteDefinition = routeDefn =>
   routeDefn.unmarshal().json(JsonLibrary.Jackson, classOf[JMap[_, _]]).end
}